+++
title    = "branko juran"
+++

Department of Mathematical Sciences <br>
Universitetsparken 5, Building 4 <br>
2100 Copenhagen Ø <br>
Office 04.4.03, `bj(at)math.ku.dk` <br>
+453532984 <br>

![image alt <](/photoNormal.jpg)


I am a third year PhD Student at the 
<a href="https://geotop.math.ku.dk/">Copenhagen Centre for Geometry and Topology</a>. My supervisor is <a href="https://web.math.ku.dk/~jg/">Jesper Grodal</a>. 

_Research Interest:_ Stable homotopy theory (equivariant, but recently also motivic), higher category theory

_Current Projects:_ Some of the following projects are in a very early stage. Please get in contact with me if you want to know about details:

Equivariant recognition principle: I am aiming to prove a version of the recognition principle for representations which do not necessarily contain a trivial summand. 

Algebraic K-theory of algebraic tori (joint with Bai, Carmeli, Riedel): Given an algebraic torus, by delooping the coweight lattice, one obtains a topological torus with an action of the Galois group. We identify the algebraic K-theory of the algebraic torus with the equivariant Bredon homology of the topological torus with coefficients the K-theory of the base field. 

Geometric fixed points of global spectra (joint with Linskens): We aim to give a description of global spectra in terms of their geometric fixed points, i.e as a partially bilax limit.
***

####  Prepublications 

[[1]] [On orthogonal factorization systems and double categories][1], preprint, 2025

We prove that the ∞-category of orthogonal factorization systems embeds fully faithfully into the ∞-category of double ∞-categories. Moreover, we prove an (un)straightening equivalence for double ∞-categories, which restricts to an (un)straightening equivalence for op-Gray fibrations and curved orthofibrations of orthogonal factorization systems. 

[[2]] [Orbifolds, Orbispaces and Global Homotopy Theory][2], preprint, 2020, to appear in Algebraic & Geometric Topology

My bachelor's thesis


[1]:  https://arxiv.org/abs/2501.01363
[2]:  https://arxiv.org/abs/2006.12374